import sys, csv
from pathlib import Path
from argparse import ArgumentParser
from sentimentja import Analyzer

analyzer = Analyzer()
_types = ["csv"]

try:
  import openpyxl as xl
  _types += ["xlsx"]
except ImportError:
  pass

# def main():
#   parser = ArgumentParser()
#   parser.add_argument("-ft", "--file-type", choices=_types, default="csv")
#   input_condition = parser.add_mutually_exclusive_group(required=True)
#   input_condition.add_argument("-is", "--input-stdin", action="store_true", default=False)
#   input_condition.add_argument("-if", "--input-file", type=Path, default=None)
#   
#   output_condition = parser.add_mutually_exclusive_group(required=True)
#   output_condition.add_argument("-os", "--output-stdout", action="store_true", default=False)
#   output_condition.add_argument("-of", "--output-file", type=Path, default=None)
#   
#   args = parser.parse_args()
#   
#   if args.input_stdin:
#     pass
#   elif not args.input_file or not args.input_file.is_file():
#     print("input file not found", sys.stderr)
#     return
#   
#   if not args.output_stdout and args.output_file.is_exists():
#     print("output file already exists", sys.stderr)
#     return
#   
#   processer = peocessers.get(args.file_type, None)
#   if processer is None:
#     print(f"file type {args.file_type} is not supported")

_FILE_VERSION = 1
_CELLINFO = ["input", 'angry', 'disgust', 'fear', 'happy', 'sad', 'surprise']

def analyze(text):
  if isinstance(text, str):
    return analyze([text])[0]
  return analyzer.analyze(text)

def stdin_processer(end_on_empty_line=False, skip_empty_line=True):
  lines = []
  while True:
    try:
      line = input("input> ")
    except EOFError:
      break
    
    line = line.strip()
    if line == "":
      if end_on_empty_line:
        break
      if skip_empty_line:
        continue
    
    lines.append(line)
  
  return lines

def xlsx_output_processer(texts, output_path, mode="w"):
  """
  Parameters
  ---
    texts: List[str]
      texts for analyze
    output_path: Path
      output path
    mode: str
      mode supports ["w", "a"]
  """
  if "xlsx" not in _types:
    raise SyntaxError("not supported type xlsx")
  
  SUPPORTED_MODES = ["w", "a"]
  
  output_path = Path(output_path)
  
  if mode not in SUPPORTED_MODES:
    raise SyntaxError(f"mode '{mode}' is not supported")
  
  row = 3
  
  if mode == "a" and output_path.is_file():
    wb = xl.load_workbook(output_path)
    ws = wb.worksheets[0]
    if ws.cell(1, 2).value != _FILE_VERSION:
      raise SyntaxError(f"_FILE_VERSION not match software {_FILE_VERSION} != file {ws.cell(1, 2).value}")
    row = ws.max_row + 1
  else:
    wb = xl.Workbook()
    ws = wb.worksheets[0]
    ws.cell(1, 1).value = "version"
    ws.cell(1, 2).value =   _FILE_VERSION
    for i, value in enumerate(  _CELLINFO, 1):
      ws.cell(2, i).value = value
  
  if 0 < len(texts):
    for i, result in enumerate(analyze(texts)):
      ws.cell(row+i, 1).value = result["sentence"]
      for emotion, value in result["emotions"].items():
        ws.cell(row+i,   _CELLINFO.index(emotion)+1).value = value
  
  wb.save(output_path)

def csv_output_processer(texts, output_path, mode="w"):
  """
  Parameters
  ---
    texts: List[str]
      texts for analyze
    output_path: Path
      output path
    mode: str
      mode supports ["w", "a"]
  """
  
  SUPPORTED_MODES = ["w", "a"]
  
  output_path = Path(output_path)
  
  if mode not in SUPPORTED_MODES:
    raise SyntaxError(f"mode '{mode}' is not supported")
  
  if mode == "a" and output_path.is_file():
    with open(output_path, newline='') as csvfile:
      reader = csv.reader(csvfile)
      for i, row in enumerate(reader):
        if i==0:
          print(row)
          if int(row[1]) != _FILE_VERSION:
            raise SyntaxError(f"_FILE_VERSION not match software {_FILE_VERSION} != file {row[1]}")
          else:
            break
            
  with open(output_path, mode, newline='') as csvfile:
    writer = csv.writer(csvfile)
    if mode != "a" or not output_path.is_file():
      writer.writerow(["version", _FILE_VERSION])
      writer.writerow(_CELLINFO)
    
    if 0 < len(texts):
      for i, result in enumerate(analyze(texts)):
        writer.writerow([result["emotions"][i] if i != "input" else result["sentence"] for i in _CELLINFO])
    
   

# if __name__ == '__main__':
#   main()
  