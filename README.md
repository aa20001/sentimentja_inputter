# sentimentja_inputter

inputter for sentiment_ja

## Prerequisites

* sentiment_ja

### Optional

* openpyxl

## Installing

```sh
pip install git+https://gitlab.com/aa20001/sentimentja_inputter.git
```

## Usage
```python
import sentimentja_inputter as inputter

lines = inputter.stdin_processer(True)
inputter.csv_output_processer(lines, "path/to/output.csv", mode="w") # mode w is override file if file exists
inputter.csv_output_processer(lines, "path/to/output.csv", mode="a") # mode a is append line if file exists

inputter.xlsx_output_processer(lines, "path/to/output.xlsx", mode="w") # xlsx output
```

