from setuptools import setup, find_packages

setup(
  name="sentimentja_inputter",
  version="0.0.1",
  packages=find_packages(),
  install_reauires=[
    "git+https://github.com/sugiyamath/sentiment_ja.git",
  ],
  extras_require={
    "xlsx": ["openpyxl"],
  },
#   entry_points={
#   	"console_scripts": [
#   	  "analyze=analyze:main",
#   	],
#   },
)